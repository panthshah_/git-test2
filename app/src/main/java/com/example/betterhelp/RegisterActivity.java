package com.example.betterhelp;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.os.Bundle;

import java.util.HashMap;
import java.util.jar.Attributes;

public class RegisterActivity extends AppCompatActivity {
    private Button registerButton;
    private EditText inputName, inputPassword,inputPhoneNumber;
    private ProgressDialog loadingBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        registerButton = (Button) findViewById(R.id.button1);
        inputName = (EditText) findViewById(R.id.editText);
        inputPhoneNumber=(EditText) findViewById(R.id.phoneNumber);
        inputPassword = (EditText) findViewById(R.id.password);
        loadingBar = new ProgressDialog(this);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateAccount();
            }
        });
    }
        private void CreateAccount() {
            String name = inputName.getText().toString();
            String phone=inputPhoneNumber.getText().toString();
            String password = inputPassword.getText().toString();

            if (TextUtils.isEmpty(name)) {
                Toast.makeText(this, "Please enter your name", Toast.LENGTH_LONG).show();
                return;
            } else if (TextUtils.isEmpty(password)) {
                Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show();
                return;
            } else if (TextUtils.isEmpty(phone)) {
                    Toast.makeText(this, "Please enter phone number", Toast.LENGTH_LONG).show();
                    return;
            } else {
                loadingBar.setTitle("Create Account");
                loadingBar.setMessage("Please Wait! Checking for the credentials");
                loadingBar.setCanceledOnTouchOutside(false);
                loadingBar.show();

                ValidatephoneNumber(name,phone,password);
    }
}
        private void ValidatephoneNumber(final String name, final String phone, final String password) {
            final DatabaseReference RootRef;
            RootRef= FirebaseDatabase.getInstance().getReference();
            RootRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                      if (!(dataSnapshot.child("Users").child(phone).exists()))
                      {
                          HashMap<String, Object> userdataMap=new HashMap<>();
                            userdataMap.put("phone", phone);
                          userdataMap.put("name", name);
                          userdataMap.put("password", password);
                          RootRef.child("Users").child(phone).updateChildren(userdataMap)
                             .addOnCompleteListener(new OnCompleteListener<Void>() {
                                 @Override
                                 public void onComplete(@NonNull Task<Void> task) {
                                     if(task.isSuccessful())
                                     {
                                         Toast.makeText(RegisterActivity.this, "Congrats your account has been created", Toast.LENGTH_SHORT).show();
                                         loadingBar.dismiss();
                                         Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                         startActivity(intent);
                                     }
                                   else
                                     {
                                         loadingBar.dismiss();
                                         Toast.makeText(RegisterActivity.this, "Network Error: Try again after some Time ", Toast.LENGTH_SHORT).show();
                                     }
                                 }
                             });

                      }
                      else
                      {
                          Toast.makeText(RegisterActivity.this, "This"+ phone+ "already exists", Toast.LENGTH_SHORT).show();
                          loadingBar.dismiss();
                          Toast.makeText(RegisterActivity.this, "Please try again using another phone number", Toast.LENGTH_SHORT).show();
                      }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
}

